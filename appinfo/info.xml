<?xml version="1.0"?>
<info>
    <id>phonetrack</id>
    <name>PhoneTrack</name>
    <description>
# PhoneTrack Owncloud application
# PhoneTrack Nextcloud application

PhoneTrack is an app to track positions of mobile devices
and display them dynamically on a Leaflet map.

Go to [PhoneTrack Crowdin project](https://crowdin.com/project/phonetrack) if you want to help to translate this app in your language.

How to use PhoneTrack :

* create a tracking session
* give the tracking URL\* to the mobile devices. Choose the [logging method](https://gitlab.com/eneiluj/phonetrack-oc/wikis/userdoc#logging-methods) you prefer.
* watch the session's devices positions in real time (or not) in PhoneTrack normal or public page

(\*) Don't forget to set the device name in the URL. Replace "yourname" with the desired device name. Setting the device name in logging app options only works with Owntracks, Traccar and OpenGTS.

On PhoneTrack main page, while watching a session, you can :

* display position history
* filter points
* manually edit/add/delete points
* edit devices (rename, change color, move to another session)
* define geofencing zones for a device
* share a session to other users (read-only)
* make a session public and share it via a public link. Positions are not visible in web logging page "publicWebLog" for private sessions.
* generate public share links with optional restrictions (filters, device name, last positions, geofencing simplification)
* import/export a session in GPX format (one file with one track by device).
* display session statistics
* [reserve a device name](https://gitlab.com/eneiluj/phonetrack-oc/wikis/userdoc#device-name-reservation) to make sure only authorized user can log with this name
* toggle session auto export (daily/weekly/monthly)
* toggle session auto purge (daily/weekly/monthly)
* restrict autozoom to some devices

Public page works like main page except there is only one session displayed, everything is read-only and there is no need to be logged in.

This app is tested with Owncloud 10 with Firefox 57+ and Chromium.
This app is tested with Nextcloud 12 and 13 with Firefox 57+ and Chromium.

This app is under development.

Link to Owncloud application website : https://marketplace.owncloud.com/apps/phonetrack

Link to Nextcloud application website : https://apps.nextcloud.com/apps/phonetrack

## Donation

I develop this app during my free time.

* Donate on Paypal : [There is a donation link here](https://gitlab.com/eneiluj/phonetrack-oc#donation) (you don't need a paypal account). 
* Bitcoin : 1FfDVdPK8mZHB84EdN67iVgKCmRa3SwF6r
* Monero : 43moCXnskkeJNf1MezHnjzARNpk2BRvhuRA9vzyuVAkTYH2AE4L4EwJjC3HbDxv9uRBdsYdBPF1jePLeV8TpdnU7F9FN2Ao

## Install

See the [AdminDoc](https://gitlab.com/eneiluj/phonetrack-oc/wikis/admindoc) for installation details.

## Known issues

* **Warning** : PhoneTrack does not work with group restriction on it. See [admindoc](https://gitlab.com/eneiluj/phonetrack-oc/wikis/admindoc#warning).

Any feedback will be appreciated.

    </description>
    <licence>AGPL</licence>
    <summary>Display phones positions in real time</summary>
    <author>Julien Veyssier</author>
    <version>0.2.8</version>
    <namespace>PhoneTrack</namespace>
    <documentation>
        <user>https://gitlab.com/eneiluj/phonetrack-oc/wikis/userdoc</user>
        <admin>https://gitlab.com/eneiluj/phonetrack-oc/wikis/admindoc</admin>
        <developer>https://gitlab.com/eneiluj/phonetrack-oc/wikis/devdoc</developer>
    </documentation>
    <category>tools</category>
    <category>social</category>
    <category>multimedia</category>
    <website>https://gitlab.com/eneiluj/phonetrack-oc</website>
    <bugs>https://gitlab.com/eneiluj/phonetrack-oc/issues</bugs>
    <screenshot>https://gitlab.com/eneiluj/phonetrack-oc/uploads/17ba0aa1f01eb5f09010fb87048d0a6a/ph1.jpeg</screenshot>
    <screenshot>https://gitlab.com/eneiluj/phonetrack-oc/uploads/55c1a459b1155cf75aba1252388c085a/ph2.jpeg</screenshot>
    <screenshot>https://gitlab.com/eneiluj/phonetrack-oc/uploads/2c88199e6ce40fcaafbb7c112f9e42f9/ph3.jpeg</screenshot>
    <dependencies>
        <database min-version="9.4">pgsql</database>
        <database>sqlite</database>
        <database min-version="5.5">mysql</database>
        <php min-version="5.6"/>
        <owncloud min-version="9.0" max-version="10.9" />
        <nextcloud min-version="9.0" max-version="14.9"/>
    </dependencies>
    <background-jobs>
        <job>OCA\PhoneTrack\Cron\AutoExport</job>
    </background-jobs>
</info>
