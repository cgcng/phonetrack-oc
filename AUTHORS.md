# Authors

* Julien Veyssier <eneiluj@posteo.net> (Developper)
* Korenisko (Slovak translations)
* Xavir (Spanish translations)
* Matheuseduardo (Brazilian Portuguese translations)
* Sebastianbacia (Polish translations)
* Mhentezari (Persian translations)
* Robml (Catalan translations)
* Xaverboss (Hungarian translations)
* Cuneyte (Turkish translations)
* @mjanssens (Dutch translations)
* @Foss_nitovf9292 (Norwegian translations)
* @AndyKl (German translations)
* @oswolf (German translations)

